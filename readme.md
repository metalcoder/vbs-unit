# VBS-Unit -- a poor man's unit testing class for VBScript

## What is VBS-Unit?

VBS-Unit is an MIT-licensed unit testing framework for VBScript. It is really 
simple and small, aiming to do one thing and do it well instead of providing 
lots of bells and whistles.

## Why unit test over Classic ASP

Classic ASP became obsolete years ago, so why develop a unit test framework for 
it?

Answer: because it is still used today.

I developed VBS-Unit to help me keep my sanity while maintaining legacy ASP 
applications at my job. It was really simple to write, and I find it extremely 
useful. For years, it just sits hidden somewhere at my hard drive. When I've had 
to fix one of these legacy apps again, I stumbled upon it, and immediately found
some enhancements to be made. So I made them, persisting everything at its own
repository. And then I thought: "this may be useful to other people". So, I'm 
sharing it.

## How to use it?

It's really simple. Just copy the `src` directory, import the `vbu.asp` file, 
and use the `vbu` class. No installation required.

### Quick explanatory example

```vbscript
<%@ Language=VBScript %>
<!--#include file="vbu/vbu.asp"-->
<!--#include file="mylist.asp"-->
<% 
dim suite
set suite = new vbu
suite.init
suite.url = "url/to/vbu" 'defaults to current directory

' Declare a new test
Sub new_list_starts_empty (suite)
    Dim list, msg
    Set list = new MyList

    msg = "The new list length should be zero."
    call suite.ok (list.Length = 0, msg)
End Sub

' Declare another test
Sub adding_items_should_increment_list_size (suite)
    Dim list, msg
    Set list = new MyList

    call list.AddItem ("bananas")
    msg = "The list length after one insertion should be 1"
    call suite.ok (list.Length = 1, msg) 

    call breadcrumbs.AddItem ("apples")
    msg = "After two insertions, the list length should be 2"
    call suite.ok (list.Length = 2, msg) 
End Sub

' Run  tests
new_list_starts_empty suite
adding_items_should_increment_list_size suite

' Print test results
response.write suite.fullReport

%>

```
