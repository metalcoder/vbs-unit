<% Option Explicit %>
<%

Class vbu
    private failed
    private successes
    private vbu_url

    public Sub init
        url = "."
        Set failed = CreateObject("System.Collections.ArrayList")
        successes = 0
    End Sub

    public Property Get url()
        url = vbu_url
    End Property

    public Property Let url(x)
        vbu_url = x
    End Property

    public Function ok(assertion, msg)
        If assertion Then
            successes = successes + 1
        Else
            failed.Add msg
        End If
        ok = assertion
    End Function

    public Property Get failures()
        failures = failed.Count
    End Property

    public Property Get total()
        total = failures + successes
    End Property

    public Property Get ratio()
        ratio = successes / total
    End Property

    public Property Get allPass()
        If failures = 0 Then
            allPass = true
        Else
            allPass = false
        End If
    End Property

    public Property Get verdict()
        If allPass Then
            verdict = "<p class='vbu-verdict vbu-success'>Succeeded</p>"
        Else
            verdict = "<p class='vbu-verdict vbu-fail'>Failed</p>"
        End If
    End Property

    public Property Get stats()
        stats = _
            "<p class='vbu-stats'>" & _
            failures & " test(s) failed, " & _
            successes & " passed." & _
            "</p>"
    End Property

    public Property Get culprits()
        Dim code, msg
        code = "<ul class='vbu-culprits'>"
        For Each msg in failed
            code = code & "<li>" & msg & "</li>"
        Next
        code = code &"</ul>"
        If allPass Then
            culprits = ""
        Else
            culprits = code
        End If
    End Property

    public Property Get inlineReport()
        inlineReport = _
            "<div class='vbu-report'>" & _
                verdict & stats & culprits & _
            "</div>"
    End Property

    private Property Get preContent()
        preContent = _
        "<!doctype html>" & _
        "<html>" & _
            "<head>" & _
                "<meta charset='utf-8'/>" & _
                headTitle & _
                favicon & _
                "<link rel='stylesheet' href='" & vbu_url & "/vbu.css'/>" & _
            "</head>"& _
            "<body>"
    End Property

    private Property Get headTitle()
        If allPass Then
            headTitle = "<title>SUCCEEDED</title>"
        Else
            headTitle = "<title>FAILED</title>"
        End If
    End Property

    private property Get favicon()
        If allPass Then
            favicon = "<link rel='shortcut icon' href='" & vbu_url & "/success.ico' type='image/x-icon'/>"
        Else
            favicon = "<link rel='shortcut icon' href='" & vbu_url & "/failed.ico' type='image/x-icon'/>"
        End If
    End Property

    private Property Get postContent()
        postContent = "<p class='vbu-footer'><a href='https://bitbucket.org/metalcoder/vbs-unit/'>VBS-Unit</a></p></body></html>"
    End Property

    public Property Get fullReport()
        fullReport = preContent & inlineReport & postContent
    End Property
End Class

%>
